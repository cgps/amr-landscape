#!/usr/bin/env bash
SCRIPT_DIR="/well/aanensen/projects/amr-landscape/shared/scripts"
INPUT_DIR="/well/aanensen/projects/amr-landscape/assemblies/escherichia_coli/batch1/assemblies/warning"
OUTPUT_DIR="/well/aanensen/projects/amr-landscape/amrfinder/test_batch"
WORKING_DIR="/well/aanensen/projects/amr-landscape/amrfinder/test_batch/scripts"
GENUS="Escherichia"
DATABASE_DIR="/well/aanensen/shared/software/amrfinder_database/2021-12-21.1"

for file in `find ${INPUT_DIR}/ -name "*.fa*" -printf "%f\n" -type f`
do
    ${SCRIPT_DIR}/generate_amrfinder_job.sh \
	${INPUT_DIR}"/"${file} \
	${OUTPUT_DIR}"/"${file}".got" \
    ${WORKING_DIR} \
	${GENUS} \
	${DATABASE_DIR} \
	> ${WORKING_DIR}"/"${file}".sh" 
done
