#!/bin/sh
#print message if less thatn 3 arguments are given
if [ $# -lt 4 ]; then
    echo "############################################################"
    echo "# This program creates a bash script to run AMRFinder on a #"
    echo "# genome. This script is ready for qsub.                   #"  
    echo "#                                                          #"
    echo "# Argument 1: Full path to fasta assembly                  #"
    echo "# Argument 2: Full path to output file                     #"
    echo "# Argument 3: Full path to working directory               #"
    echo "# Argument 4: Genus of the genome                          #"
    echo "############################################################" 
    exit 1
fi

#define parameters which are passed in.
INPUT_PATH=$1
OUTPUT_PATH=$2
WORKING_PATH=$3
GENUS=$4
DATABASE_PATH=$5

infile_name=`basename ${INPUT_PATH}`
infile_name=`echo "${infile_name%.*}"`

#outfile_dir=`dirname ${OUTPUT_PATH}`

#define the template
cat  <<EOF
#!/bin/bash
#$ -wd ${WORKING_PATH}
#$ -P aanensen.prjc
#$ -N ${infile_name}
#$ -l h_vmem=8G
#$ -q short.qc

#module use /apps/eb/testing/\$MODULE_CPU_TYPE/modules/all
module load AMRFinder/3.10.23-foss-2021b


amrfinder  --plus -d ${DATABASE_PATH} -n ${INPUT_PATH} -O ${GENUS} > ${OUTPUT_PATH}

EOF
