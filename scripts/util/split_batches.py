#!/usr/bin/env python3
import more_itertools
import sys
import os

if len(sys.argv) != 4:
    sys.exit("Please supply path to id_file, number of batches and starting batch number \nsplit_batches.py <ID FILE> <NUM BATCHES> <STARTING BATCH NUM>\n e.g split_batches.py ids.txt 10 3")
id_filepath = sys.argv[1]
num_batches=int(sys.argv[2])
starting_batch_number=int(sys.argv[3])

if not os.path.exists(id_filepath):
    sys.exit(f"The file {id_filepath} does not exist")

with open(id_filepath) as id_file:
    ids = id_file.read().splitlines()


batches = more_itertools.divide(num_batches,ids)

for index, _ in enumerate(range(num_batches)):
    batch_output_filepath = f"batch{starting_batch_number + index}_ids.txt"
    if os.path.exists(batch_output_filepath):
        sys.exit(f"The file {batch_output_filepath} already exists.\nStopping!!\n")
    with open(batch_output_filepath, "w") as output_file:
        for sample_id in batches[index]:
            output_file.write(f"{sample_id}\n")