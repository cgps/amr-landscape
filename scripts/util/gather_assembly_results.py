import pandas as pd
import sys

# This program collecs the qualifyr_reports.tsv from multiple batches and
# combines them in a single file.

def get_df(qc_reports):
    # Gather information from all batches into a single dataframe
    df_list = []
    for i in qc_reports:
        df_list.append(pd.read_csv(i, sep="\t"))
    df = pd.concat(df_list, axis=0, ignore_index=True)
    return df


qc_reports = sys.argv[1:len(sys.argv)-1]
df = get_df(qc_reports)

df.to_csv(sys.argv[-1], sep="\t", index=False)