import pandas as pd
import sys
import json

# This program filters genomes based on QC metrics. It takes one input
# and that is the qualifyr_report.tsv produced by the GHRU pipeline.

#Load quality_summary table
qc_file = sys.argv[1]
df = pd.read_csv(qc_file,"\t")


def apply_filter(df, metric, f_type, f_value):
    
    if f_type == "min_max":
        metric_min, metric_max = f_value
        return df[(df[metric]>=metric_min) & (df[metric]<=metric_max)]
    elif f_type == "max":
        return df[df[metric]<=f_value]
    elif f_type == "min":
        return df[df[metric]>=f_value]
    elif f_type == "contains":
        f_value = [x.lower() for x in f_value]
        return df[df[metric].str.contains('|'.join(f_value),case=False)]
    else:
        print("Only 'min_max', 'min', and 'max' can be used as f_type")

def get_qc_filters(qc_file):
    with open(qc_file, 'r') as f:
        data = json.load(f)

    filters = data["filters"]
    result = []
    for f in filters:
        temp = []
        temp.append(f)
        temp.append(filters[f]["type"])
        value = filters[f]["value"] if isinstance(filters[f]["value"], int) else tuple(filters[f]["value"])
        temp.append(value)
        result.append(temp)
    return result

filtering = get_qc_filters(sys.argv[2])

#filtering = [
#            ["confindr.percentage_contamination.metric_value","max",5],
#            ["quast.GC (%).metric_value","min_max", (56.35,57.98)],
#            ["quast.# contigs.metric_value","max", 500],
#            ["quast.Total length.metric_value","min_max", (4969898,6132846)],
#            ["bactinspector.species.metric_value","in",["Klebsiella","Raoultella"]]
#            ]

#filtering = [
#            ["quast.Total length.metric_value","min_max", (4969898,6132846)]
#            ]

print("# Original size: "+str(len(df)))
for filter in filtering:
    df = apply_filter(df, filter[0], filter[1], filter[2])    
    print("# Size after '"+filter[0]+"' filter: "+str(len(df)))

out_file = sys.argv[3]
df.to_csv( out_file, sep="\t", index=False)