#!/usr/bin/env python3
import sys
import os
import glob
import click

if len(sys.argv) != 3:
    sys.exit("Please supply path to id_file and fastq directory\nfind_missing_ids.py <ID FILE> <FASTQ DIR>")
id_filepath = sys.argv[1]
fastq_dir = sys.argv[2]

if not os.path.exists(id_filepath):
    sys.exit(f"The file {id_filepath} does not exist")

if not os.path.exists(fastq_dir):
    sys.exit(f"The directory {fastq_dir} does not exist")

with open(id_filepath) as id_file:
    ids = id_file.read().splitlines()
    fastq_files = [
        os.path.basename(fastq_file)
        for fastq_file in 
        glob.glob(os.path.join(fastq_dir, "*_1.fastq.gz"))
    ]
    fastq_file_ids = [
        fastq_file.replace("_1.fastq.gz", "").split("_")[-1]
        for fastq_file in fastq_files
    ]
    missing_ids = set(ids).symmetric_difference(set(fastq_file_ids))
    if len(missing_ids) > 0:
        output_filepath = f"{os.path.splitext(id_filepath)[0]}_missing_ids.txt"
        if click.confirm(f"There are {len(missing_ids)} missing ids.\nDo you want to write these to {output_filepath}?", default = True):
            if os.path.exists(output_filepath):
                if not click.confirm(f"Output file {output_filepath} already exists.Do you want to continue?", default=False):
                    sys.exit(0)
            with open(output_filepath, "w") as output_file:
                for missing_id in missing_ids:
                    output_file.write(f"{missing_id}\n")
        
