#!/usr/bin/env bash
AMRFINDER_RESULTS_DIR="/well/aanensen/projects/amr-landscape/shared/scripts"
LONG_FORM_OUT_FILE="amrfinder_results_long.csv"
SIMPLE_FORM_OUT_FILE_DIR="amrfinder_results_simple.csv"

SCRIPT_PATH="/well/aanensen/projects/amr-landscape/shared/scripts/run_amrfinder_gatherer.sh"

python $SCRIPT_PATH $AMRFINDER_RESULTS_DIR $LONG_FORM_OUT_FILE $SIMPLE_FORM_OUT_FILE_DIR