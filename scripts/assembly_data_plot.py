import pandas as pd
import sys
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as mticker
from matplotlib import rcParams

STD_NUM = 3
ZERO_PH = 0.1

if len(sys.argv) <2:
    print("First argument QC file and Second argument name of output file")


# Figure size in inches
rcParams['figure.figsize'] = 9.7,8.27

def get_df(qc_file):
    df = pd.read_csv(qc_file, sep="\t")
    return df


#Function that draws each plot
def set_plot(df, metric, is_biological, axs, std_colours, pos, log_y, log_x):
    # Basic plot
    mean = df[metric].median()
    std = df[metric].mad()
    
    if log_x:
        x =sns.histplot(data=df, x=metric, fill=True,ax=axs[pos],log_scale=True )
    else:
        x =sns.histplot(data=df, x=metric, fill=True,ax=axs[pos],log_scale=False )
    
    # Log scale axes
    if log_y:
        x.set_yscale('log')
    
    x.yaxis.set_major_formatter(mticker.ScalarFormatter())
    x.xaxis.set_major_formatter(mticker.ScalarFormatter())
    plt.ticklabel_format(style='plain', axis='y')
    # Standard deviation plot
    if is_biological:
        for i in range(1,STD_NUM+1):
            x.axvline(mean-(i*std), color=colour_std[i-1], linestyle='--')
            x.axvline(mean+(i*std), color=std_colours[i-1], linestyle='--')

# Gather information from all batches into a single dataframe
df = get_df(sys.argv[1])

df.loc[df["quast.# N's per 100 kbp.metric_value"] == 0.0,
        "quast.# N's per 100 kbp.metric_value"] = ZERO_PH


# Setup plot grid
fig, axs = plt.subplots(nrows=6)
colour_std = ["red","orange","green"]

# Metrics list
metrics_list = [("quast.GC (%).metric_value",True,False,False),
                ("quast.Total length.metric_value",True,False,False),
                ("quast.# contigs.metric_value",False,False,True),
                ("quast.N50.metric_value",False,False,True),
                ("quast.# N's per 100 kbp.metric_value",False,False,True),
                ("confindr.percentage_contamination.metric_value",False,False,False)]

# Setup plots
pos = 0
for metric, zero_min, log_y, log_x in metrics_list:
        set_plot(df, metric, zero_min, axs, colour_std, pos, log_y, log_x)
        pos = pos+1

# Draw plots
plt.subplots_adjust(hspace = 0.6, top=0.97, bottom=0.06, right=0.95, left=0.1)
plt.savefig(sys.argv[2])
plt.show()
