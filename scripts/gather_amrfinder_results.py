from os.path import basename, splitext, isfile, join
import pandas as pd
from datetime import datetime
import sys
from os import listdir

def now():
    now = datetime.now()
    return now.strftime("%Y-%m-%d %H:%M:%S")

print("\033[92m \033[1m "+now()+" \033[0m AMRFinder Gathering script.")

# Directory containing all AMRDinder results
#INPUT_DIR = "/well/aanensen/projects/amr-landscape/amrfinder/escherichia_coli/batch1/pass"
INPUT_DIR = sys.argv[1]
# Output file of long form
#LONG_OUT = "amrfinder_results_long.csv"
LONG_OUT = sys.argv[2]
# Output file of simplified form
#SIMPLE_OUT = "amrfinder_results_simple.csv"
SIMPLE_OUT = sys.argv[3]

# List of files in that directory
file_names = [f for f in listdir(INPUT_DIR) if isfile(join(INPUT_DIR, f))]


# Create dataframe from each AMRFidner result
frames = []
for in_file in file_names:

    df = pd.read_csv(INPUT_DIR+"/"+in_file, sep="\t")
    # Choose only rows with AMR in 'Element type' (acquired resistance)
    df = df[df["Element type"] == "AMR"]

    # Add name of isolate ('run_accession') to dataframe
    name = splitext(splitext(basename(in_file))[0])[0]
    df["accession_id"] = name

    frames.append(df)
print("\033[92m \033[1m "+now()+" \033[0m Loaded "+str(len(frames))+" AMRFinder results.")
    
# Join all dataframes in a big one
result = pd.concat(frames)

cols = list(result.columns)
cols = [cols[-1]] + cols[:-1]
result = result[cols]
result.to_csv(LONG_OUT, index=False)
print("\033[92m \033[1m "+now()+" \033[0m Concatenated AMRFinder results.")

# Print out antibiotics tree
class_u = result["Class"].unique()
#for cu in class_u:
#    print("|--"+cu)
#    temp = result[result["Class"]==cu]
#    subc = temp["Subclass"].unique()
#    for sub in subc:
#        print("|  |--"+sub)
print("\033[92m \033[1m "+now()+" \033[0m Loaded "+str(len(class_u))+" ABX subclasses.")

# Get list of Subclasses of antibiotics
subclasses = result["Subclass"].unique()
# Get list of accession ids
ids = list(result["accession_id"].unique())


# Crate list of headers including all subclasses in dataset
header_list = []
header_list.append("accession_id")
for subclass in subclasses:
    header_list.append(subclass)
    #header_list.append(subclass+"__colour")


df_array = []
# For each accession id in dataframe
for row in ids:
    t = []
    t.append(row)
    
    # Get slice of table corresponding to that accession_id
    temp = result[result["accession_id"]==row]
    # For each subclass in dataframe
    for subclass in subclasses:
        # Get slice of table for each subclass 
        sub_temp = temp[temp["Subclass"]==subclass]
        # If the table is empty, the accession_id doesnt carry a gene 
        # associated with the current subclass.
        if len(sub_temp) == 0:
            t.append("none")
        else:
            gene = list(sub_temp["Gene symbol"])
            gene = gene.sort()
            t.append(",".join(gene))
    df_array.append(t)

print("\033[92m \033[1m "+now()+" \033[0m AMRFinder results simplified.")

final_df = pd.DataFrame(df_array, columns = header_list)
final_df.to_csv(SIMPLE_OUT, index=False)
print("\033[92m \033[1m "+now()+" \033[0m Simple Format Printed.")
