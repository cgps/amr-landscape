# Running AMRFinder

## 0. Setting up environment
_This step needs to be performed only once_

### a. Add shared private modules to bashrc
Add this line to your `~/.bashrc` file

```bash
export MODULEPATH=/well/aanensen/shared/privatemodules:$MODULEPATH
```

### b. Source modified bashrc
Run this script

```bash
source ~/.bashrc
```

## 1. Running AMRFInder on a genome
### 1a. Script to create a submittable job
The script `generate_amrfinder_job.sh` creates a bash script that can be submitted to the scheduler with qsub. It takes 4 arguments:
1. The full path to an assembled genome in fasta format
2. The full path to the output file 
3. The full path to the working directory. This is useful to debug the output logs.
4. The genus of the genome of interest

Running this script prints the submittable code to screen.

```bash
#!/bin/sh
#print message if less thatn 3 arguments are given
if [ $# -lt 4 ]; then
    echo "############################################################"
    echo "# This program creates a bash script to run AMRFinder on a #"
    echo "# genome. This script is ready for qsub.                   #"  
    echo "#                                                          #"
    echo "# Argument 1: Full path to fasta assembly                  #"
    echo "# Argument 2: Full path to output file                     #"
    echo "# Argument 3: Full path to working directory               #"
    echo "# Argument 4: Genus of the genome                          #"
    echo "############################################################" 
    exit 1
fi

#define parameters which are passed in.
INPUT_PATH=$1
OUTPUT_PATH=$2
WORKING_PATH=$3
GENUS=$4

infile_name=`basename ${INPUT_PATH}`
infile_name=`echo "${infile_name%.*}"`

#outfile_dir=`dirname ${OUTPUT_PATH}`

#define the template
cat  <<EOF
#!/bin/bash
#$ -wd ${WORKING_PATH}
#$ -P aanensen.prjc
#$ -N ${infile_name}
#$ -l h_vmem=8G
#$ -q short.qc


module load Anaconda3/2020.11
eval "\$(conda shell.bash hook)"
conda activate AMRFinder

amrfinder --plus -n ${INPUT_PATH} -O ${GENUS} > ${OUTPUT_PATH}

EOF
```

### 1b. Example
The following example runs AMRFinder on the Escherichia genome located at `/well/aanensen/projects/amr-landscape/assemblies/escherichia_coli/batch1/assemblies/pass/ERX1229853_ERR1156327.fasta`, writes the output to `/well/aanensen/projects/amr-landscape/amrfinder/ERX1229853_ERR1156327.fasta.got`, saves logs in `/well/aanensen/projects/amr-landscape/amrfinder`. Finally, it saves the script in `amrfinder_job.sh`.

```bash
bash generate_amrfinder_job.sh \
/well/aanensen/projects/amr-landscape/assemblies/escherichia_coli/batch1/assemblies/pass/ERX1229853_ERR1156327.fasta \
/well/aanensen/projects/amr-landscape/amrfinder/ERX1229853_ERR1156327.fasta.got \
/well/aanensen/projects/amr-landscape/amrfinder \
Escherichia > amrfinder_job.sh
```

This job can be submitted with:

```bash
qsub amrfinder_job.sh
```

## 2. Running AMRFInder on all files in a directory
### 2a. Script to create a batch of submittable job
The script `run_generator.sh` allows creating a batch of submittable scripts given a directory containing fasta files. A copy of this script can be copied and the following variables should be modified.
1. `INPUT_DIR`: Directory containing all the fasta-formatted genomes to be processed with AMRFinder. All these genomes should share the same genus.
2. `OUPUT_DIR`: Directory where the outpit of AMRFinder will be saved
3. `WORKING_DIR`: Directory where execution and AMRFinder logs will be saved.
4. `GENUS`: Genus of these genomes.

```bash
#!/usr/bin/env bash
SCRIPT_DIR="/well/aanensen/projects/amr-landscape/shared/scripts"

INPUT_DIR="/well/aanensen/projects/amr-landscape/assemblies/escherichia_coli/batch1/assemblies/warning"
OUTPUT_DIR="/well/aanensen/projects/amr-landscape/amrfinder/test_batch"
WORKING_DIR="/well/aanensen/projects/amr-landscape/amrfinder/test_batch/scripts"
GENUS="Escherichia"

for file in `find ${INPUT_DIR}/*.fa* -type f -printf "%f\n"`
do
    ${SCRIPT_DIR}/generate_amrfinder_job.sh \
	${INPUT_DIR}"/"${file} \
	${OUTPUT_DIR}"/"${file}".got" \
       	${WORKING_DIR} \
	${GENUS} \
	> ${WORKING_DIR}"/"${file}".sh" 
done
```

### 2b. Running script
Running the script as it is will create a submittable job for each fasta-formatted genome in `/well/aanensen/projects/amr-landscape/assemblies/escherichia_coli/batch1/assemblies/warning`. The results of these jobs will be saved at `/well/aanensen/projects/amr-landscape/amrfinder/test_batch`, while the logs will be saved at `/well/aanensen/projects/amr-landscape/amrfinder/test_batch/scripts`.

```bash
bash run_generator.sh
```

### 2c. How I structure my directories
In this structure, `batch1` hold the results of AMRFinder, and `scripts` holds the submittable jobs and the execution logs. I also save the medified copy of `run_generator.sh` in the `scripts` directory


```bash
|--amrfinder
|  |--escherichia_coli
|  |  |---batch1
|  |  |   |--scripts
```

### 2d. Submit all jobs 
Go to the directory holding all submittable jobs and submit as in:

```bash
for i in `*.sh`;do qsub $i;done
```